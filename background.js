//Identificador de notificaciones
var notIDN = 0;
var notID = 0;


//Si se pulsa en el botonm te lleva a la ventana de condiguracion
chrome.notifications.onButtonClicked.addListener(
	function buttonCallback (notID, btnIndex) {
		if (notID == "errCFG") {
			chrome.runtime.openOptionsPage();
		}
	}
);

// Create a notification
function muestraNoti (popID, percent, estado, texto, fncallback){
	var titulo = '';
	var accion = '';
	var tipo = '';
	var progreso = 0;

	//Segun el estado, diferentes configuraciones
	switch (estado){
		case 1:
			titulo = 'Terminado';
			notID = popID;
			tipo = 'basic';
			break;
		case 2:
			titulo = 'Procesando';
			notID = 'id' + notIDN++; //inicializa el id
			tipo = 'progress';
			break;
		case 3:
			titulo = '¡Ojo cuidao!';
			accion = 'Ir a configuración';
			notID = 'errCFG';
			tipo = 'basic';
			break;
		case 4:
			titulo = 'Procesando';
			notID = popID;
			tipo = 'progress';
			progreso = percent;
			break;
		default:
			titulo = '¡¡¡Error!!!';
			notID = 'err';
			tipo = 'basic';
	}

	//Opciones basicas
	var options = {
		type: tipo,
		title: titulo,
		message: texto,
		iconUrl: 'refresh.png'		
	}

	//Casos particulares
	if (estado == 2 || estado == 3){
		if (estado == 3){
			//Si esl estado es 3, necesita un acceso a la configuracion
			options.buttons= [
					{ title: accion, iconUrl: "refresh.png" },
				];
		}
		chrome.notifications.create(notID, options, function (notID){
			if (fncallback && typeof(fncallback) === "function"){fncallback();}
			if (estado == 3){
				setTimeout(function() {
					if (isNaN(notID)){notID = 0;}
					chrome.notifications.clear(notID);
				}, 5000);
			}
		});
	}else{
		if (estado == 4){
			options.progress = progreso;
		}
		chrome.notifications.update(notID, options, function (notID){
			if (fncallback && typeof(fncallback) === "function"){fncallback();}
			if (estado == 0 || estado == 1){
				setTimeout(function() {
					chrome.notifications.clear(notID);
				}, 5000);
			}
		});
	}
	
	//Devolvemos el id de la notificacion
	return notID;
	
}

var CCapiKey = '';
var CCoutF = '';

//Se define el solo una vez el conversor
chrome.runtime.onInstalled.addListener(function(){
	var menuItem = {
		"id": "conversor",
		"title": "Conversor",
		"contexts": ["link", "video"]
	};	
	chrome.contextMenus.create(menuItem);
});

//Cada vez que le pinchemos
chrome.contextMenus.onClicked.addListener(function(clickData){
	chrome.storage.sync.get('SCO', function(items) {
		if (items.SCO) {
			CCapiKey = items.SCO.CCapiKey;
			CCoutF = items.SCO.CCoutF;
			
			if (CCapiKey == '' && CCoutF == ''){
				var msg="Por favor, configura la extensión";
				muestraNoti(null,null,3,msg,null);
			}else{
				var nombre = clickData.srcUrl.substring(clickData.srcUrl.lastIndexOf("/")+1,clickData.srcUrl.lastIndexOf(".")-1);
				var Fnombre = clickData.srcUrl.substring(clickData.srcUrl.lastIndexOf("/")+1,clickData.srcUrl.length);
				var msg = 'Video procesandose: '+ nombre;
				var popID = muestraNoti(null,null,2,msg, function(){
					fetch('https://api.cloudconvert.com/v2/jobs',{
						method: "POST",
						headers:{
							"Accept": "application/json",
							'Content-Type': 'application/json',
							"Authorization": "Bearer "+CCapiKey
						},				
						body: JSON.stringify({tasks: {import1:{operation:'import/url', url:clickData.srcUrl, filename:Fnombre}, Tarea:{operation:'convert', input:['import1'], output_format:'mp4'}, export1:{operation:'export/url', input:['Tarea'], inline:false, archive_multiple_files:false}}, tag:'jobbuilder'})
						//body: JSON.stringify({tasks: {import1:{operation:'import/url', url:'https://webmlovers.xyz/files/16534.78fecd39.webm', filename:'16534.78fecd39.webm'}, Tarea:{operation:'convert', input:['import1'], output_format:'mp4'}, export1:{operation:'export/url', input:['Tarea'], inline:false, archive_multiple_files:false}}, tag:'jobbuilder'})						
						}).then(response => response.json())
						.then(result => {
							console.log('Success:', result);
								var completado = false;
								var id = 0;
								(function estado() {
									fetch(result.data.tasks[id].links.self,
									{
										method: "GET",
										headers:{
											"Accept": "application/json",
											"Content-Type": "application/json",
											"Authorization": "Bearer "+CCapiKey
										}
									}).then(response => response.json())
									.then(result => {
										if (result.data.status == 'error'){
											var msg = result.data.message;
											completado = true;
											muestraNoti(popID,null,1,msg,null);
										}else{
											// Cuando este al 100% se descarga, si no, vuelve a preguntar por el estado
											if (result.data.status != 'finished'){
												var msg = 'Video procesandose: '+ nombre;
												muestraNoti(popID,Math.round(result.data.percent /3),4,msg,null);
												setTimeout(estado, 1000);
											}else{
												id = id+1;												
												completado = true;
												if (completado == true && id == 3){
													var msg = 'Coversion realizada correctamente: '+ result.data.result.files[0].filename;
													muestraNoti(popID,null,1,msg, function(){
														chrome.downloads.download({url: result.data.result.files[0].url});
														/*var link = document.createElement("a");
														link.download = result.data.result.files[0].filename;
														link.href = result.data.result.files[0].url;
														link.click();*/
													});
												}else{
													completado = false;
													setTimeout(estado, 1000);
												}
											}
										}
									})
									.catch(error => {
									  console.error('Error:', error);
									});
								})();
						})
						.catch(error => {
						  console.error('Error:', error);
						});
					});
			}
		}else{
			//Si no tenemos configurada la extension
			var msg="Por favor, configura la extensión";
			muestraNoti(null,null,3,msg,null);
		}
	});
});
